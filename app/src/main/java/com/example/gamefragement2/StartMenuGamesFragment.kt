package com.example.gamefragement2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.gamefragement2.databinding.FragmentStartMenuGamesBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [StartGameMenuFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StartGameMenuFragment : Fragment() {
    private lateinit var binding:FragmentStartMenuGamesBinding
    private lateinit var startMenuGamesViewModel: StartMenuGamesViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_start_menu_games, container,false)
        startMenuGamesViewModel = ViewModelProvider(this).get(StartMenuGamesViewModel::class.java)
        binding.startMenuGamesViewModel = startMenuGamesViewModel

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_start_menu_games, container,
            false)
//        binding.plusMain.setOnClickListener{
//            it.findNavController().navigate(R.id.action_startGameMenuFragment_to_plusGamesFragment)
//        }
//        binding.minusMain.setOnClickListener {
//            it.findNavController().navigate(R.id.action_startGameMenuFragment_to_minusFragment)
//        }
//        binding.multiMain.setOnClickListener {
//            it.findNavController().navigate(R.id.action_startGameMenuFragment_to_multiFragment).
//        }


        startMenuGamesViewModel.eventPlus.observe(viewLifecycleOwner, Observer { hasNext ->

            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_startGameMenuFragment_to_plusGamesFragment)
                startMenuGamesViewModel.changeToPlusFinish()

            }


        })
        startMenuGamesViewModel.eventMinus.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_startGameMenuFragment_to_minusFragment)
                startMenuGamesViewModel.changeToMinusFinish()
            }

        })

        startMenuGamesViewModel.eventMulti.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_startGameMenuFragment_to_multiFragment)
                startMenuGamesViewModel.changeToMultiFinish()
            }

        })
        return binding.root
    }


}
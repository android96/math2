package com.example.gamefragement2

import android.os.Build.VERSION_CODES.M
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.gamefragement2.databinding.FragmentMultiGamesBinding
import com.example.gamefragement2.databinding.FragmentPlusGamesBinding
import kotlinx.android.synthetic.main.fragment_plus_games.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [plusGamesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MultiFragment : Fragment() {

    private lateinit var  binding: FragmentMultiGamesBinding
    private lateinit var multiGamesViewModel: MultiGamesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_multi_games, container,false)
        multiGamesViewModel = ViewModelProvider(this).get(MultiGamesViewModel::class.java)
        binding.multiGamesViewModel = multiGamesViewModel

        multiGamesViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            Log.i("test","test")
            if(hasNext){
                enableAllButton()
                binding.invalidateAll()
                multiGamesViewModel.onNextFinish()
            }

        })

        multiGamesViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_multiFragment_to_startGameMenuFragment)
                multiGamesViewModel.changeToHomeFinish()
            }

        })

        multiGamesViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGamesViewModel.onSelectChoice1Finish()
            }

        })

        multiGamesViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGamesViewModel.onSelectChoice2Finish()
            }

        })

        multiGamesViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                multiGamesViewModel.onSelectChoice3Finish()
            }

        })

        return binding.root
    }

    fun disabledAllButton(){
        binding.apply {
            btnPlus1.isEnabled =false
            btnPlus2.isEnabled =false
            btnPlus3.isEnabled =false
        }
    }
    fun enableAllButton(){
        binding.apply {
            btnPlus1.isEnabled =true
            btnPlus2.isEnabled =true
            btnPlus3.isEnabled =true
        }
    }


}
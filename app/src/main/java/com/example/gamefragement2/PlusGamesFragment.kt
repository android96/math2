package com.example.gamefragement2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.gamefragement2.databinding.FragmentPlusGamesBinding
import kotlinx.android.synthetic.main.fragment_plus_games.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [plusGamesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class plusGamesFragment : Fragment() {

    private lateinit var  binding: FragmentPlusGamesBinding
    private lateinit var plusGameViewModel: PlusGamesViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_plus_games, container,false)
        plusGameViewModel = ViewModelProvider(this).get(PlusGamesViewModel::class.java)
        binding.plusGameViewModel = plusGameViewModel

        plusGameViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            Log.i("test","test")
            if(hasNext){
                enableAllButton()
                binding.invalidateAll()
                plusGameViewModel.onNextFinish()
            }

        })

        plusGameViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_plusGamesFragment_to_startGameMenuFragment)
                plusGameViewModel.changeToHomeFinish()
            }

        })

        plusGameViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                plusGameViewModel.onSelectChoice1Finish()
            }

        })

        plusGameViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                plusGameViewModel.onSelectChoice2Finish()
            }

        })

        plusGameViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                plusGameViewModel.onSelectChoice3Finish()
            }

        })

        return binding.root
    }

    fun disabledAllButton(){
        binding.apply {
            btnPlus1.isEnabled =false
            btnPlus2.isEnabled =false
            btnPlus3.isEnabled =false
        }
    }
    fun enableAllButton(){
        binding.apply {
            btnPlus1.isEnabled =true
            btnPlus2.isEnabled =true
            btnPlus3.isEnabled =true
        }
    }






}
package com.example.gamefragement2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.gamefragement2.databinding.FragmentMinusGamesBinding
import com.example.gamefragement2.databinding.FragmentPlusGamesBinding
import kotlinx.android.synthetic.main.fragment_plus_games.*
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [plusGamesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MinusFragment : Fragment() {

    private lateinit var  binding: FragmentMinusGamesBinding
    private lateinit var minusGamesViewModel: MinusGamesViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_minus_games, container,false)
        minusGamesViewModel = ViewModelProvider(this).get(MinusGamesViewModel::class.java)
        binding.minusGamesViewModel = minusGamesViewModel

        minusGamesViewModel.eventNextQuestion.observe(viewLifecycleOwner, Observer { hasNext ->
            Log.i("test","test")
            if(hasNext){
                enableAllButton()
                binding.invalidateAll()
                minusGamesViewModel.onNextFinish()
            }

        })

        minusGamesViewModel.eventHome.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                view?.findNavController()?.navigate(R.id.action_minusFragment_to_startGameMenuFragment)
                minusGamesViewModel.changeToHomeFinish()
            }

        })

        minusGamesViewModel.eventSelectChoice1.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                minusGamesViewModel.onSelectChoice1Finish()
            }

        })

        minusGamesViewModel.eventSelectChoice2.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                minusGamesViewModel.onSelectChoice2Finish()
            }

        })

        minusGamesViewModel.eventSelectChoice3.observe(viewLifecycleOwner, Observer { hasNext ->
            if(hasNext){
                disabledAllButton()
                binding.invalidateAll()
                minusGamesViewModel.onSelectChoice3Finish()
            }

        })

        return binding.root
    }

    fun disabledAllButton(){
        binding.apply {
            btnPlus1.isEnabled =false
            btnPlus2.isEnabled =false
            btnPlus3.isEnabled =false
        }
    }
    fun enableAllButton(){
        binding.apply {
            btnPlus1.isEnabled =true
            btnPlus2.isEnabled =true
            btnPlus3.isEnabled =true
        }
    }


}